# Viscosity of methyl cellulose solutions in RT-DC

This repository contains data files and analysis tools to recreate the plots 
for Reichel and Büyükurganci 2023. "Effective viscosity of methyl cellulose 
solutions in phosphate buffered saline in real-time deformability cytometry": 
https://arxiv.org/abs/2302.01664.


The `raw_data` folder contains datafiles, which were used for the analysis.

The folder `plots` contains the figure panels.

`imgs` contains images that are used for illustration in the notebooks.

To run the notebooks an installation of python>=3.10 is needed.

The `requirements.txt` and `environment.yml` contain all dependencies for the notebooks to work and can be installed
either with pip or conda. Please create an virtual environment, e.g. with Anaconda's `conda create` command and run the following commands:

Create a conda virtual environment from the `environment.yml` by running:

```
conda env create -f environment.yml
```

The default environment name will be `mc_rheo_py310`

Or from the requirements.txt:


```
# using Conda
conda create --name <env_name> --file requirements.txt

# using pip
pip install -r requirements.txt

```
